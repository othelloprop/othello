package othello.player;

import othello.logic.OthelloData;

/**
 * Interficie pet tal de estandaritzar els jugadors.
 * @author PropGrup
 */
public interface Jugador {
    /**
     * Funcio que que calcula el seguent moviment
     * @param t Informacio de tauler de joc
     * @param jugador Informacio de jugador
     * @return retorna les coordenades del seguent moviment
     */
    public int[] moviment(OthelloData t, int jugador);
    
    /**
     * funcio que retorna el nom del jugador
     * @return nom del jugador
     */
    public String nom();
}
