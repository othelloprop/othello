package othello.player;

import othello.logic.OthelloData;
import othello.logic.OthelloMove;
import java.util.ArrayList;
import java.util.List;
import static othello.player.Constants.HEURISTIC_CORNERS;
import static othello.player.Constants.HEURISTIC_EARLYGAME;
import static othello.player.Constants.HEURISTIC_ERLYGAME_NUMERO_MOVIMENTS;
import static othello.player.Constants.HEURISTIC_ERLYGAME_VALOR_CASELLES;
import static othello.player.Constants.HEURISTIC_FINALGAME_NUMERO_FITXES;
import static othello.player.Constants.HEURISTIC_LATEGAME;
import static othello.player.Constants.HEURISTIC_LATEGAME_NUMERO_FITXES;
import static othello.player.Constants.HEURISTIC_MIDGAME;
import static othello.player.Constants.HEURISTIC_MIDGAME_NUMERO_FITXES;
import static othello.player.Constants.HEURISTIC_MIDGAME_NUMERO_MOVIMENTS;
import static othello.player.Constants.HEURISTIC_MIDGAME_NUMERO_REVERSE;
import static othello.player.Constants.HEURISTIC_MIDGAME_VALOR_CASELLES;
import static othello.utils.Constants.GAME_SIZE;

/**
 * Classe que implementa un jugador amb la estrategia MinMax
 * @author PropGrup
 */
public class MinMax implements Jugador {
    private String nom;
    private int prof;
    private final int[][] pesos = {
            { 200, -100, 100,  50,  50, 100, -100,  200 },
            { -100, -200, -50, -50, -50, -50, -200, -100 },
            { 100,  -50, 100,   0,   0, 100,  -50,  100 },
            { 50,  -50,   0,   0,   0,   0,  -50,   50 },
            { 50,  -50,   0,   0,   0,   0,  -50,   50 },
            { 100,  -50, 100,   0,   0, 100,  -50,  100 },
            { -100, -200, -50, -50, -50, -50, -200, -100 },
            { 200, -100, 100,  50,  50, 100, -100,  200 }
        };
    
    /**
     * Constructor de classe
     * @param name Variable que conte el nom del jugador
     * @param prof Variable que conte la profunditat del minmax
     */
    public MinMax(String name, int prof){
        this.nom = name;
        this.prof = prof;
    }
    
    /**
     * Funcio que retorna el nom del jugador
     * @return String que conte el nom del jugador.
     */
    @Override
    public String nom(){
        return this.nom;
    }
    
    /**
     * Funcio que calcula el valor potencial d'un tauler.
     * @param t OthelloData que conte la informacio del tauler
     * @param color Int que identifica al jugador
     * @return retorna el valor potencial del tauler
     */
    public int calcHeuristica(OthelloData t, int color){
        List<OthelloMove> llista_mov = new ArrayList<>(t.llistaMoviments(color).values());
        if (t.getContador() <= HEURISTIC_EARLYGAME) {
        // Principi del joc
            return HEURISTIC_ERLYGAME_NUMERO_MOVIMENTS * contMov(t, color, llista_mov)
                + HEURISTIC_ERLYGAME_VALOR_CASELLES * valorTile(t, color, llista_mov)
                + HEURISTIC_CORNERS * calcCorner(t, color);
        }
        else if (t.getContador()  <= HEURISTIC_MIDGAME) {
            // Migdeljoc del joc
            return HEURISTIC_MIDGAME_NUMERO_FITXES * difTile(t, color)
                + HEURISTIC_MIDGAME_NUMERO_MOVIMENTS * contMov(t, color, llista_mov)
                + HEURISTIC_MIDGAME_NUMERO_REVERSE * countPosReverse(t, color,llista_mov)
                + HEURISTIC_MIDGAME_VALOR_CASELLES * valorTile(t, color,llista_mov)
                + HEURISTIC_CORNERS * calcCorner(t, color);
        }else if(t.getContador()  <= HEURISTIC_LATEGAME){
            return HEURISTIC_LATEGAME_NUMERO_FITXES * difTile(t, color)
                + HEURISTIC_CORNERS * calcCorner(t, color);
        }
        else {
            return HEURISTIC_FINALGAME_NUMERO_FITXES * difTile(t, color);
        }
    }
    
    /**
     * Funcio per tal d'asignar un pes a cada casella
     * @param t Informacio del tauler
     * @param color Identificador del jugador
     * @param llista_mov Llista del moviments possibles per al tauler donat
     * @return retorna el valor potencial calculat
     */
    private int valorTile(OthelloData t, int color, List<OthelloMove> llista_mov){
        int result=0;
        int[][] tauler=t.getTauler();
        for(OthelloMove mov : llista_mov){
            if(tauler[mov.coord.i][mov.coord.j] == color)
                result+=pesos[mov.coord.i][mov.coord.j];
        }
        return result;
    }
    
    /**
     * Funcio que calcula les fitxes que es podran arribar a girar.
     * @param t Informacio del tauler
     * @param color Identificador del jugador
     * @param llista_mov Llista de moviments possibles
     * @return Retrona el numero de fitxes que es podrien arribar a girar
     */
    private int countPosReverse(OthelloData t, int color, List<OthelloMove> llista_mov){
        int result=0;
        for(OthelloMove mov : llista_mov)
            result += mov.llistareverse.size();
        return 100 * result;
    }
    
    
    /**
     * Funcio que calcula la diferencia de fitxes entre els jugadors
     * @param t Informacio del tauler
     * @param color Informacio del jugador
     * @return Diferencia de fitxes entre un jugador i altre.
     */
    private int difTile(OthelloData t, int color){
        int fitxa1=0;
        int fitxa2=0;
        int[][] tauler = t.getTauler();
        for (int i=0; i<GAME_SIZE; i++){
            for (int j=0; j<GAME_SIZE; j++){
                if(tauler[i][j] == color )
                    fitxa1++;
                else if(tauler[i][j] == -color )
                    fitxa2++;
            }
        }
        
        return 100 * (fitxa1 - fitxa2) / (fitxa1 + fitxa2);
    }
    
    /**
     * Funcio que contas el numero de moviments possibles
     * @param t Informacio del tauler
     * @param color Identificador del color
     * @param llista_mov Llista de moviments possibles
     * @return Retorna el numero de moviments possibles
     */
    private int contMov(OthelloData t, int color, List<OthelloMove> llista_mov){
        return 100 * llista_mov.size();
    }
    
    private int calcCorner(OthelloData t, int color){
        int[][] tauler = t.getTauler();
        int result = 0;
        
        if(tauler[0][0]==color){
            result++;
        }
        else  if(tauler[0][0]==-color){
            result--;
        }
        
        if(tauler[0][7]==color) result++;
        else  if(tauler[0][7]==-color) result--;
        
        if(tauler[7][0]==color) result++;
        else  if(tauler[7][0]==-color) result--;
        
        if(tauler[7][7]==color) result++;
        else  if(tauler[7][7]==-color) result--;
        
        return 100 * result;
    }
    
    /**
     * Part del minmax que calcula els fills i maximitza el resultat
     * @param t Tauler de joc
     * @param profunditat Profunditat a la que arribara el graf
     * @param alfa Valor de alfa per tal de comparar
     * @param beta Valor de beta per tal de comparar
     * @param color Identificador del jugador
     * @return Retorna el valor del fill mes gran.
     */
    public int valorMax(OthelloData t, int profunditat, int alfa, int beta, int color){
        List<OthelloMove> llista_mov = new ArrayList<>(t.llistaMoviments(color).values());
        if(llista_mov.isEmpty() || profunditat > this.prof)
            return calcHeuristica(t, color);
        for(OthelloMove mov : llista_mov){
            OthelloData taux = new OthelloData(t);
            taux.afegeix(mov.coord.i, mov.coord.j, color);
            alfa = Math.max(alfa, valorMin(taux,profunditat+1,alfa,beta,-color));
            if(alfa>=beta) return beta;    
        }
        return alfa;
    }
    
    /**
     * Part del minmax que calcula els fills i minimitza el resultat
     * @param t Tauler de joc
     * @param profunditat Profunditat a la que arribara el graf
     * @param alfa Valor de alfa per tal de comparar
     * @param beta Valor de beta per tal de comparar
     * @param color Identificador del jugador
     * @return Retorna el valor del fill mes petit.
     */
    public int valorMin(OthelloData t, int profunditat, int alfa, int beta, int color){
        List<OthelloMove> llista_mov = new ArrayList<>(t.llistaMoviments(color).values());
        if(llista_mov.isEmpty() || profunditat > this.prof)
            return calcHeuristica(t, color);
        for(OthelloMove mov : llista_mov){
            OthelloData taux = new OthelloData(t);
            taux.afegeix(mov.coord.i, mov.coord.j, color);
            beta = Math.min(beta, valorMax(taux,profunditat+1,alfa,beta,-color));
            if(alfa>=beta) return alfa;
        }  
        return beta;
    }
    
        /**
         * Funcio que calcula el seguent moviment
         * @param t Informacio del tauler
         * @param color Identificador de jugador
         * @return retorna el moviment a realitzar
         */
    @Override
    	public int[] moviment(OthelloData t, int color)
	{
            float max = 0;
            float max_act;
            int[] millor_mov = new int[2];
            OthelloMove aux; 
            List<OthelloMove> llista_mov = new ArrayList<>(t.llistaMoviments(color).values());
            if(llista_mov.isEmpty()) return null;
            System.out.println("Llista mov"+llista_mov);
            
            aux = llista_mov.get(0);
            millor_mov[0] = aux.coord.i;
            millor_mov[1] = aux.coord.j;
            for (OthelloMove mov : llista_mov){
                OthelloData taux = new OthelloData(t);
                taux.afegeix(mov.coord.i, mov.coord.j, color);
                max_act = valorMin(taux,0,-500, 500,-color);
                if (max_act > max) {
                            max = max_act;
                            millor_mov[0] = mov.coord.i;
                            millor_mov[1] = mov.coord.j;
                }
            }
            System.out.println("Llista mov"+millor_mov[0]+millor_mov[1]);
            return millor_mov;
	}
}
