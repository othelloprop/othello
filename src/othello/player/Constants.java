/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.player;

/**
 *
 * @author aleixabengochea
 */
public final class Constants {

    private Constants() {
            // restrict instantiation
    }

    public static final int HEURISTIC_CORNERS = 100000 ;
    public static final int HEURISTIC_EARLYGAME = 20 ;
    public static final int HEURISTIC_MIDGAME = 58 ;
    public static final int HEURISTIC_LATEGAME = 62 ;
    
    public static final int HEURISTIC_ERLYGAME_NUMERO_MOVIMENTS= 5;
    public static final int HEURISTIC_MIDGAME_NUMERO_MOVIMENTS = 2; 
    
    public static final int HEURISTIC_ERLYGAME_VALOR_CASELLES = 20;
    public static final int HEURISTIC_MIDGAME_VALOR_CASELLES = 10;
    
    public static final int HEURISTIC_MIDGAME_NUMERO_FITXES = 10;
    public static final int HEURISTIC_LATEGAME_NUMERO_FITXES = 500;
    public static final int HEURISTIC_FINALGAME_NUMERO_FITXES = 100000;
    
    public static final int HEURISTIC_MIDGAME_NUMERO_REVERSE = 2;

}