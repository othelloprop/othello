/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.player;

import othello.logic.OthelloData;

/**
 * Classe que implementa al jugador manual.
 * @author PropGrup
 */
public class Manual implements Jugador{
    String name;
    int x,y;
    int game_height, game_width;
    
    /**
     * Constructor de classe
     * @param nom Nom que tindra el jugador
     */
    public Manual(String nom){
        this.name = nom;
    }
    
    /**
     * Funcio que calcula el seguent moviment esta pensada per ser reimplementada
     * ja que no tenim la x e y del grafic.
     * @param tauler Informacio del tauler
     * @param jugador Identificador del jugador
     * @return retorna un null
     */
    @Override
    public int[] moviment(OthelloData tauler, int jugador){
        int[] aux = new int[2];
        int Ymax = game_height;
        int Xmax = game_width;
        int stepx = (int) Xmax/10;
        int stepy = (int) Ymax/10;
        aux[1] = x/stepx -1;
        aux[0] = y/stepy -1;
        return aux;
    }
    
    public void setSize(int x, int y){
        this.game_height=x;
        this.game_width=y;
    }
    public void setX(int x){
        this.x=x;
    }
    public void setY(int y){
        this.y=y;
    }
   
    /**
     * Retorna el nom del jugador
     * @return 
     */
    public String nom(){
        return this.name;
    }
}
