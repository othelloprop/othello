package othello.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import othello.logic.OthelloData;
import othello.logic.OthelloMove;

/**
 * Classe que impleneta al jugador aleatori
 * @author PropGrup
 */
public class Random implements Jugador{
    String name;
    HashMap<String,OthelloMove> hash_mov;
    
    /**
     * Constructor de classe
     * @param nom Nom que identifica el jugador
     */
    public Random(String nom){
        this.name = nom;
    }
    
    /**
     * Funcio que escolleix un dels moviments possibles aleatoriament
     * @param tauler Tauler
     * @param jugador Identificador del jugador
     * @return retorna les coordenades i,j on es fara el moviment
     */
    @Override
    public int[] moviment(OthelloData tauler, int jugador){
        int[] pos = new int[2];
        hash_mov = tauler.llistaMoviments(jugador);
        
        //Transformem en una llista per tal de poder agafar un aleatori.
        List<OthelloMove> llista_mov = new ArrayList<>(hash_mov.values());
        //System.out.println("Jugador: "+jugador+ "llista: "+ mov);
        if(llista_mov.isEmpty()) return null;
        int randomNum = ThreadLocalRandom.current().nextInt(0, llista_mov.size());
        //System.out.println(randomNum);
        
        
        pos[0] = llista_mov.get(randomNum).coord.i;
        pos[1] = llista_mov.get(randomNum).coord.j;
        
        return pos;   

    }
    
    /**
     * Funcio per obtenir el nom del jugador
     * @return retorna el nom del jugador
     */
    @Override
    public String nom(){
        return this.name;
    }
}
