package othello.logic;

import java.util.ArrayList;

/**
 * Classe per tal de estructurar un possible moviment
 * @author PropGrup
 */
public class OthelloMove {
    
    public Punt coord;
    //Variable que conte la llista de caselles que es giraran si es fa el moviment
    public ArrayList<Punt> llistareverse;
    
    /**
     * Constructor de clase
     * @param col Identifica columna del moviment
     * @param row Identifica fila del moviment
     */
    public OthelloMove(int col, int row){ //int dirx, int diry){
        llistareverse = new ArrayList<>();
        coord = new Punt(col,row);
    }
    
    /**
    * Funcio que permet imprimir per pantalla la classe
    * @return 
    */
    @Override
    public String toString(){
        return "{ pos("+this.coord.i+","+this.coord.j +" }";//+") dir("+this.dirx+","+this.diry+") }";
    }
    
}
