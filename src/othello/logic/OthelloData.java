/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.logic;

import othello.logic.OthelloMove;
import java.util.ArrayList;
import java.util.HashMap;
import static othello.utils.Constants.GAME_SIZE;
/**
 *  Classe que conte tota la estructura i informacio per jugar al othello
 * @author PropGrup
 */
public class OthelloData {
    
    //Array que conte el tauler de joc.
    private int[][] tauler;
    private int contador;
    //private HashMap<String,ArrayList<OthelloMove>> llista_mov;
    private HashMap<String,OthelloMove> mov_jug1 = null;
    private HashMap<String,OthelloMove> mov_jug2 = null;
   
    
    /**
     * Constructor que genera un nou tauler
     */
    public OthelloData(){
        
        //Inicialitzacio automatica.
       /* this.tauler = new int[8][8];
        for (int i=0; i<8; i++)
            for (int j=0; j<8; j++)
                this.tauler[i][j] = 0;
        this.tauler[3][3] = -1;
        this.tauler[4][4] = -1;
        this.tauler[3][4] = 1;
        this.tauler[4][3] = 1;*/
        
        //Inicialitzacio manual, per tal de que sigui mes facil veure i
        //modificar a l'hora de debugear.
        tauler = new int[][] {
            { 0, 0, 0, 0, 0, 0, 0, 0},
            { 0, 0, 0, 0, 0, 0, 0, 0},
            { 0, 0, 0, 0, 0, 0, 0, 0},
            { 0, 0, 0,-1, 1, 0, 0, 0},
            { 0, 0, 0, 1,-1, 0, 0, 0},
            { 0, 0, 0, 0, 0, 0, 0, 0},
            { 0, 0, 0, 0, 0, 0, 0, 0},
            { 0, 0, 0, 0, 0, 0, 0, 0}
        };
        
        //El contador de caselles ara conte 4
        contador=4;
        
    }
    
    /**
     * Constructor que genera un nou tauler a partir d'un anterior
     * @param t 
     */
    public OthelloData(OthelloData t){
        tauler = new int[GAME_SIZE][GAME_SIZE];
        contador = t.getContador();
        int[][] ant_tauler = t.getTauler();
        
        for (int i=0; i<GAME_SIZE; i++)
            for (int j=0; j<GAME_SIZE; j++)
                tauler[i][j] = ant_tauler[i][j];
    }
    
    /**
     * Funcio que retorna el nombre de fitxes ficades al tauler
     * @return numero de fitxes
     */
    public int getContador(){
        return contador;
    }
    
    /**
     * Getter del tauler.
     * @return retorna una array de ints amb el tauler de joc.
     */
    public int[][] getTauler(){
        return tauler;
    }
    /**
     * Funcio que permet printar per pantalla el tauler actual.
     */
    public void printTauler(){
        for(int i=0; i<GAME_SIZE; i++){
            for(int j=0; j<GAME_SIZE; j++)
                System.out.printf("% d", tauler[i][j]);
            System.out.print("\n");
        }
        System.out.print("\n");
    }
    
    /**
     * Funcio que retorna una llista del moviments possibles per al jugador
     * passat per parametre.
     * @param jugador Indica el jugador per al qual es vol llistar el moviments.
     * @return Hashmap de OthelloMove. Identifica cada moviment possible.
     */
    public HashMap<String,OthelloMove> llistaMoviments(int jugador){
        //ArrayList que contindra la llista de possibles moviments.
        
        //Dos bucles que recorren la matriu sencera
        HashMap<String,OthelloMove> llista_mov = new HashMap<>();
        for(int i=0; i<GAME_SIZE; i++){
            for(int j=0; j<GAME_SIZE; j++){
                //Si trobem una casella en blanc mirem:
                if(tauler[i][j] == 0){
                    //Per cada casella en blanc, mirem las del voltant
                    //X X X
                    //X 0 X
                    //X X X
                    
                    for(int x=-1; x<2; x++){
                        //Comprovem que no ens sortim dels limits de la matriu
                        if(i+x >= 0 && i+x < GAME_SIZE){
                            for(int y=-1; y<2;y++){
                                //Comprovem que no ens sortim dels limits de la matriu
                                if(j+y >=0 && j+y<GAME_SIZE){
                                   //Variables que guarden la direccio.
                                    int dirx = x;
                                    int diry = y;
                                    //Si a la casella trobem una fitxa contraria
                                    OthelloMove mov = new OthelloMove(i,j);
                                    if(tauler[i+dirx][j+diry] == -jugador){
                                        while(i+dirx != GAME_SIZE && i+dirx != -1 && j+diry != GAME_SIZE && j+diry != -1){
                                            if(tauler[i+dirx][j+diry] == 0){
                                                break;
                                            }else if(tauler[i+dirx][j+diry] == jugador){
                                                if(llista_mov.containsKey(String.valueOf(i)+String.valueOf(j))){
                                                    llista_mov.get(String.valueOf(i)+String.valueOf(j)).llistareverse.addAll(mov.llistareverse);
                                                }else
                                                    llista_mov.put(String.valueOf(i)+String.valueOf(j), mov );
                                            
                                                break;
                                            }else{
                                                mov.llistareverse.add(new Punt(i+dirx, j+diry));
                                                dirx+=x;
                                                diry+=y;
                                            }
                                        }
                                    }   
                                }
                                        
                            }
                        }
                    }       
                }
            }
        } 
        setHashMap(llista_mov, jugador);
        return llista_mov;
    }
    
    
    
    /**
     * Funcio que retorna el hashmap de un jugador
     * @param jugador identificador de jugador
     * @return retorna el hashmap
     */
    public HashMap<String,OthelloMove> getHashMap(int jugador){
        return jugador == 1 ? mov_jug1 : mov_jug2;
    }
    
    /**
     * Funcio que serveix per a guardar el hashmpa
     * @param moves Hashmap a guardar
     * @param jugador Identificador del jugador on guardar
     */
    private void setHashMap(HashMap<String,OthelloMove> moves, int jugador){
        if(jugador == 1)
            mov_jug1 = moves;
        else
            mov_jug2 = moves;
    }
    
    /**
     * Funcio que afegeix una fitxa al tauler
     * @param i identifica la fila
     * @param j identifica la columna
     * @param jugador identifica la fitxa a posar
     * @return Si s'ha pogut afegir retorna true, sino false
     */
    public boolean afegeix(int i, int j, int jugador){
        boolean afegit=false;
        HashMap<String,OthelloMove> llista_mov = getHashMap(jugador);
        if (llista_mov == null){
            llista_mov = llistaMoviments(jugador);
        } 
        if(llista_mov.containsKey(String.valueOf(i)+String.valueOf(j))){
            tauler[i][j] = jugador;
            afegit=true;
            contador++;
            for(Punt p : llista_mov.get(String.valueOf(i)+String.valueOf(j)).llistareverse)
                tauler[p.i][p.j] = jugador;
        }
        
        setHashMap(null, jugador);
        return afegit;
    }
    
    /**
     * Funcio que retorna qui es el guanyador
     * @return retorna el gunyador o 0 si hi ha empat
     */
    public int solucio(){
        int jug1 = 0;
        int jug2 = 0;
        for(int i=0; i<GAME_SIZE; i++)
            for(int j=0; j<GAME_SIZE; j++)
                switch (tauler[i][j]){
                    case 1:
                        jug1++;
                        break;
                        
                    case -1:
                        jug2++;
                        break;
                }
        if(jug1 > jug2) return 1; 
        else if(jug2 > jug1) return -1;
        else return 0;
    }
    

    
}
