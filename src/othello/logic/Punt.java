package othello.logic;

/**
 * Classe estandar per guardar x,y
 * @author PropGrup
 */
public class Punt{
        public int i,j;
        /**
         * Construcor de classe
         * @param i i a guardadr
         * @param j j a guardar
         */
        public Punt(int i, int j){
            this.i = i;
            this.j = j;
        }
    }