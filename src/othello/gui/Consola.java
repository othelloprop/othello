package othello.gui;

/**
 *  Interface per tal de estandaritzar el tipus de 
 * @author PropGrup
 */
public interface Consola {
    /**
     * Per tal de poder fer mes d'un jpanel diferent, nomes cal implementar
     * Consumer per tal de que StreamCapturer pugui ser genric
     * @param text Text que s'imprimira per el output
     */
    public void appendText(String text); 
}
