package othello.gui;

import javax.swing.JTabbedPane;

/**
 *  Classe que representa el panel amb finestres.
 * Nomes ja una implementada, ja que en un principi voliem afegir estadisticas
 * i un log generic.
 * @author aleixabengochea
 */
public class TabPanel extends JTabbedPane {
    private static final ConsolaJPanel CAPTURE_PANE = new ConsolaJPanel();
    public TabPanel(){
        super();
        add(CAPTURE_PANE, "Console");
        //add("Log", new JButton());
    }
    public static ConsolaJPanel getCapturePane(){
        return CAPTURE_PANE;
    }
}
