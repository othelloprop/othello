/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.gui;

/**
 * Constants que utilitzarem a la part grafica
 * @author aleixabengochea
 */
public final class Constants {

    private Constants() {
            // restrict instantiation
    }
    //Constants que serveixen per mostrar els combobox
    public static final String[] TYPE_STRINGS = { "Manual", "Random", "MinMax"};
    public static final String[] COLOR_STRINGS = {"Blanc", "Negre", "Verd", "Vermell", "Blau"};
 
    public static final String COLS = "ABCDEFGH ";

}