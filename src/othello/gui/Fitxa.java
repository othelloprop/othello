/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import javax.swing.JPanel;

/**
 * Classe que implementa la forma de la fitxa que s'utilitza al grafic
 * @author PropGrup
 */
public class Fitxa extends JPanel {
  protected static final int FOCUS_STROKE = 3;
  protected Color color;
  protected Shape shape;
  protected Shape border;
  protected Shape base;
  
  public Fitxa(Color color) {
    super();
  }
  
  
  //Definicio de la base, borde i forma
  protected void initShape() {
    if (!getBounds().equals(base)) {
            base = getBounds();
            shape = new Ellipse2D.Double(0, 0, getWidth() - 1, getHeight() - 1);
            
            border = new Ellipse2D.Double(FOCUS_STROKE, FOCUS_STROKE,
                                          getWidth() - 1 - FOCUS_STROKE * 2,
                                          getHeight() - 1 - FOCUS_STROKE * 2);
        }
  }
  
  //Funcio que dibuixa la fitxa
  @Override protected void paintComponent(Graphics g) {
        initShape();
        Graphics2D g2 = (Graphics2D) g.create();
        //Per a que es vegi decent.
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setColor(color);
        g2.fill(shape);    
  }
  
  //Funcio que ens permet definir un borde
  @Override protected void paintBorder(Graphics g) {
    initShape();
    Graphics2D g2 = (Graphics2D) g.create();
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setColor(getForeground());
    g2.draw(shape);
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_OFF);
    g2.dispose();
  }
  
  //Fem un override per tal de poder ficar el size quan construim el tauler
  @Override public Dimension getPreferredSize() {
        Dimension d = super.getPreferredSize();
        int s = Math.max(d.width, d.height);
        d.setSize(s, s);
        return d;
    }
  
  //Funcio que permetrar canviar de color segons el jugador
  public void setColor(Color c){
      this.color = c;
      repaint();
  }
}