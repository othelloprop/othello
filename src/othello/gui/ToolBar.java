package othello.gui;

import javax.swing.JButton;
import javax.swing.JToolBar;

/**
 *  Class que conte el toolbar principal del joc.
 * @author aleixabengochea
 */
public class ToolBar extends JToolBar{
    public JButton jugar = new JButton("Nova Partida");
    public JButton pasar = new JButton("Pasar torn");
    public JButton settings = new JButton("Settings");
    
    /**
     * Constructor de classe
     */
    public ToolBar(){
        super();
        setFloatable(false);
        
        add(jugar);
        add(pasar);
        add(settings);
        addSeparator();

    }
}
