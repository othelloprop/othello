/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Classe que permet imprimir per un jpanel el output de la consola
 * @author Propgrup
 */
public class ConsolaJPanel extends JPanel implements Consola {

    private JTextArea output;

    /**
     * Constructor de classe
     */
    public ConsolaJPanel() {
        setLayout(new BorderLayout());
        output = new JTextArea();
        output.setCaretPosition(output.getDocument().getLength());
        JScrollPane scrollPane = new JScrollPane(output);
        scrollPane.setPreferredSize(new Dimension(500, 100));
        add(scrollPane);
    }

    
    /**
     * Funcio que implementem de la interficie, per tal de mostrar el text per
     * pantalla
     * @param text Text que volem mostrar
     */
    @Override
    public void appendText(final String text) {
        if (EventQueue.isDispatchThread()) {
            output.setText(output.getText()+text);
        } else {

            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    appendText(text);
                }
            });

        }
    }        
}
