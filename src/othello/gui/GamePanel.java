package othello.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import static othello.gui.Constants.COLS;


/**
 * Classe que conte el tauler de joc representat graficament
 * @author aleixabengochea
 */
public class GamePanel extends JPanel{
    public Fitxa[][] othelloTiles = new Fitxa[8][8];
    public Color color_jug1;
    public Color color_jug2;
    
    /**
     * Constructor del joc
     */
    public GamePanel(){
        super(new GridLayout(10, 10));

        setBackground(new Color(76,145,65));
        setBorder(new LineBorder(Color.BLACK));
        
        Insets buttonMargin = new Insets(0,0,0,0);
        for (int ii = 0; ii < othelloTiles.length; ii++) {
            for (int jj = 0; jj < othelloTiles[ii].length; jj++) {
             
                Fitxa b = new Fitxa(Color.GREEN);
                b.setPreferredSize(new Dimension(32,32));
                b.setBorder(BorderFactory.createEmptyBorder());
                b.setVisible(false);
                
                othelloTiles[jj][ii] = b;
            }
        }

        //fill the chess board
        add(new JLabel(""));
        // fill the top row
        for (int ii = 0; ii < 8; ii++) {
            add(
                    new JLabel(COLS.substring(ii, ii + 1),
                    SwingConstants.CENTER));
        }
        add(new JLabel("", SwingConstants.CENTER));
        // fill the black non-pawn piece row
        for (int ii = 0; ii < 8; ii++) {
            for (int jj = 0; jj < 8; jj++) {
                switch (jj) {
                    case 0:
                        add(new JLabel("" + (ii + 1),
                                SwingConstants.CENTER));
                    
                    default:
                        
                        add(othelloTiles[jj][ii]);
                        
                }    
            }
            add(new JLabel("" + (ii + 1),
                                SwingConstants.CENTER));
        }
        add(new JLabel("", SwingConstants.CENTER));
        for (int ii = 0; ii < 8; ii++) {
            add(
                    new JLabel(COLS.substring(ii, ii + 1),
                    SwingConstants.CENTER));
        }
        add(new JLabel("", SwingConstants.CENTER));
        
    }
    
    /**
     * Funcio que permet representar les caselles dels jugadors.
     * @param tauler matriu que conte els jugadors per tal de representarlos
     */
    public void drawTiles(int[][] tauler){
        for(int i=0; i<8; i++)
            for(int j=0; j<8; j++){
                switch(tauler[i][j]){
                    case 1:
                        othelloTiles[j][i].setColor(color_jug1);
                        othelloTiles[j][i].setVisible(true);
                        break; 
                    case -1:
                        othelloTiles[j][i].setColor(color_jug2);
                        othelloTiles[j][i].setVisible(true);
                        break;
                    case 0:
                        othelloTiles[j][i].setVisible(false);
                        break;
                }
            }
    }
    /*
    public void drawPosMove(HashMap<String,OthelloMove> dict){
        List<OthelloMove> llista_mov = new ArrayList<>(dict.values());
        for(OthelloMove move : llista_mov)
            othelloTiles[j][i].setColor(color_jug1);
    }*/
    
}
