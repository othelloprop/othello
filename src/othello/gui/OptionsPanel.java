/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.gui;

import java.awt.Dimension;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import static othello.gui.Constants.COLOR_STRINGS;
import static othello.gui.Constants.TYPE_STRINGS;

/**
 *
 * @author aleixabengochea
 */
public class OptionsPanel extends JPanel{
    //ComboBox
    
    private final JLabel jug1 = new JLabel("===== Jugador1 =====");
    private final JLabel jug2 = new JLabel("===== Jugador2 =====");
    
    private final JLabel tipus_label1 =  new JLabel("Tipus: ");
    private final JLabel tipus_label2 =  new JLabel("Tipus: ");
    
    private final JLabel nom_label1 =  new JLabel("Nom: ");
    private final JLabel nom_label2 =  new JLabel("Nom: ");
    
    private final JLabel color_label1 =  new JLabel("Color: ");
    private final JLabel color_label2 =  new JLabel("Color: ");
    
    private final JLabel deep_label1 =  new JLabel("Profunditat: ");
    private final JLabel deep_label2 =  new JLabel("Profunditat: ");
    
    public JTextField nom_text1 =  new JTextField("Jugador1");
    public JTextField nom_text2 =  new JTextField("Jugador2");
    
    public JTextField deep_text1 =  new JTextField("4");
    public JTextField deep_text2 =  new JTextField("4");
    
    public JComboBox color_box1 =  new JComboBox(COLOR_STRINGS);
    public JComboBox color_box2 =  new JComboBox(COLOR_STRINGS);
    
    public JComboBox tipus_box1 = new JComboBox(TYPE_STRINGS);
    public JComboBox tipus_box2 = new JComboBox(TYPE_STRINGS);
    

    public OptionsPanel(){
        super();
        GroupLayout layout =  new GroupLayout(this);
        this.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        

                
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(jug1)
                    
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tipus_label1)
                        .addComponent(tipus_box1))
                        
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(color_label1)
                        .addComponent(color_box1))
                        
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(nom_label1)
                        .addComponent(nom_text1))
                        
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(deep_label1)
                        .addComponent(deep_text1))

                        
                    .addComponent(jug2) 
                        
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tipus_label2)
                        .addComponent(tipus_box2))
                
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(color_label2)
                        .addComponent(color_box2))
                
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(nom_label2)
                        .addComponent(nom_text2))
                
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(deep_label2)
                        .addComponent(deep_text2))
                 
                
        );
        layout.setVerticalGroup(
                layout.createSequentialGroup()
                    .addComponent(jug1)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                         .addComponent(tipus_label1)
                         .addComponent(tipus_box1))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(color_label1)
                        .addComponent(color_box1))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(nom_label1)
                        .addComponent(nom_text1))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(deep_label1)
                        .addComponent(deep_text1))
                        
                    .addComponent(jug2)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                         .addComponent(tipus_label2)
                         .addComponent(tipus_box2))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(color_label2)
                        .addComponent(color_box2))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(nom_label2)
                        .addComponent(nom_text2))
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(deep_label2)
                        .addComponent(deep_text2))
);
  
        color_box2.setSelectedIndex(1);
        setPreferredSize(new Dimension(200, 20));
        setVisible(true);
    }
}
