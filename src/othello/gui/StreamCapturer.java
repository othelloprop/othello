/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package othello.gui;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Classe que connecta el sistem. (outputstram) amb el capture panel
 * per tal de redirigir el text
 * @author aleixabengochea
 */
public class StreamCapturer extends OutputStream {

    private final StringBuilder buffer;
    private String prefix;
    private final Consola consumer;
    private final PrintStream old;

    /**
     * Constructor de classe
     * @param consumer Panel on volem enviar
     * @param old El stream anterior per on enviavem les dades, per tal
     * de imprimir per els dos llocs, el matenim.
     */
    public StreamCapturer(Consola consumer, PrintStream old) {
        buffer = new StringBuilder(128);
        this.old = old;
        this.consumer = consumer;
    }

    /**
     * Funcio que escriu al buffer.
     * @param b Caracter que volem enviar. (Es un buffer)
     * @throws IOException 
     */
    @Override
    public void write(int b) throws IOException {
        char c = (char) b;
        String value = Character.toString(c);
        buffer.append(value);
        if (value.equals("\n")) {
            consumer.appendText(buffer.toString());
            buffer.delete(0, buffer.length());
        }
        old.print(c);
    }        
}