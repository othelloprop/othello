package othello.main;

import othello.player.MinMax;
import othello.logic.OthelloData;
import othello.player.Random;
import othello.player.Manual;
import othello.player.Jugador;
import othello.gui.StreamCapturer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.PrintStream;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import othello.gui.GamePanel;
import othello.gui.OptionsPanel;
import othello.gui.TabPanel;
import othello.gui.ToolBar;

/**
 * Classe principal on s'ajunten el model i la vista i s'implementa tota la 
 * mecanica del joc
 * @author PropGrup
 */
public final class Tauler {
    
    //Variables que conte el joc i jugadors
    OthelloData tauler;
    Jugador jug2;
    Jugador jug1;
    
    //Variable per tal de seleccionar el color
    private Color[] colors = {Color.white, Color.black, Color.green, Color.red, Color.blue};
    //Variables per al control del joc∫
    int notorn;
    static  boolean torn;
    int [] coord;
    
    //Variables gui del joc
    static JPanel gui;
    static ToolBar tools;
    static OptionsPanel settings;
    static GamePanel othelloBoard;
    static TabPanel tabbedPane;
    static JFrame frame;
    
    static boolean showsettings=true;
    
    /**
     * Constructor de classe, genera la part grafica i el OthelloData
     */
    public Tauler(){
        initGui();
        initJoc();
    }
    
    /**
     * Funcio que inicialitza la estructura i mecanica del joc
     */
    public void initJoc(){
        tauler = new OthelloData();
        
        //Obtenim la configuracio del panel setting i generem els nous jugadors.
        switch(settings.tipus_box1.getSelectedIndex()){
            case 0:
                jug1 = new Manual(settings.nom_text1.getText());
                break;
            case 1:
                jug1 = new Random(settings.nom_text1.getText());
                break;
            case 2:
                jug1 = new MinMax(settings.nom_text1.getText(), Integer.parseInt(settings.deep_text1.getText()));
                break;
            
        }
        switch(settings.tipus_box2.getSelectedIndex()){
            case 0:
                jug2 = new Manual(settings.nom_text2.getText());
                break;
            case 1:
                jug2 = new Random(settings.nom_text2.getText());
                break;
            case 2:
                jug2 = new MinMax(settings.nom_text2.getText(), Integer.parseInt(settings.deep_text2.getText()));
                break;     
        }
        
        //Afegim el color seleccionat com a color per a les fitxes
        othelloBoard.color_jug1=colors[settings.color_box1.getSelectedIndex()];
        othelloBoard.color_jug2=colors[settings.color_box2.getSelectedIndex()];

        //Variables per al control del flow
        notorn=0;
        torn=true;
        coord = new int[2];
    }
    
    
    /**
     * Funcio que inicialitza el frame i afegeix tots els components
     */
    public static void initGui() {
        // GUI Principal
        gui = new JPanel(new BorderLayout());
        gui.setBorder(new EmptyBorder(0, 0, 0, 0));
        
        //Inicialitzem cadascun dels components que portara el panell principal
        tools = new ToolBar();
        settings = new OptionsPanel();
        othelloBoard = new GamePanel();
        tabbedPane = new TabPanel();
        
        
        //Afegim les coses al panel principal
        gui.add(tools, BorderLayout.PAGE_START);
        gui.add(settings, BorderLayout.EAST);
        gui.add(tabbedPane, BorderLayout.SOUTH);
        gui.add(othelloBoard);
        
        //Creem el frame de la aplicacio i afegim el panel principal
        frame = new JFrame("Othello");
        frame.add(gui);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationByPlatform(true);
                
        frame.pack();
        frame.setMinimumSize(frame.getSize());       
        frame.setVisible(true);
    }
    
    /**
     * Funcio que porta tota la mecanica del joc
     */
    public void torn(){
        boolean afegit=false;
        if(tauler.getContador()==64 || notorn>=2){
            othelloBoard.drawTiles(tauler.getTauler());
            System.out.println("Joc acabat!");
            System.out.println("Guanyador: "+ tauler.solucio());
        }
        else{
            
            //Try catch per poder saber si el jugador es manual
            try{
                Manual jug = (Manual) jug1;
                jug.setSize(othelloBoard.getWidth(), othelloBoard.getHeight());
                jug.setX(coord[0]);
                jug.setY(coord[1]);
            }catch(ClassCastException err){}
 
            try{
                Manual jug = (Manual) jug2;
                jug.setSize(othelloBoard.getWidth(), othelloBoard.getHeight());
                jug.setX(coord[0]);
                jug.setY(coord[1]);
            }catch(ClassCastException err){}
            
            if(torn){
                coord=jug1.moviment(tauler, 1);
                if (coord != null){
                    afegit=tauler.afegeix(coord[0], coord[1], 1);
                    notorn=0;
                    System.out.println("Torn -> "+jug2.nom());
                }else{
                    notorn++;
                    afegit=true;
                    System.out.println("Salta Torn -> "+jug1.nom());
                }
            }else{
                coord=jug2.moviment(tauler, -1);
                if (coord != null){
                    afegit=tauler.afegeix(coord[0], coord[1], -1);
                    notorn=0;
                    System.out.println("Torn -> "+jug1.nom());
                }else{
                    notorn++;
                    afegit=true;
                    System.out.println("Salta Torn -> "+jug2.nom());
                }
            }

            if(afegit){
                torn=!(torn);
                //Modifiquem la visual del tauler;
                othelloBoard.drawTiles(tauler.getTauler());
                //Imprimim el tauler per consola per tal de debugar
                //tauler.printTauler();
            }else{
                System.out.println("Moviment no valid, torna a tirar. ("+coord[0]+","+coord[1]+")");
            }
        }
    }
   
    /**
     * Funcio main, principal que executara el programa
     * @param args possibles arguments
     */
    public static void main(String[] args) {
        Tauler joc = new Tauler();
        othelloBoard.drawTiles(joc.tauler.getTauler());
        
        //Afegim la funcionalitat al boto tornar a jugar
        tools.jugar.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        joc.initJoc();
                        othelloBoard.drawTiles(joc.tauler.getTauler());
                    }
                });
        tools.pasar.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        joc.torn=!(joc.torn);
                        joc.notorn++;
                    }
                });
        tools.settings.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        settings.setVisible(showsettings);
                        tabbedPane.setVisible(showsettings);
                        showsettings=!(showsettings);
                    }
                });
        
        //Afegim la funcionalitat al panel del joc per tal de poder jugar manualment
        othelloBoard.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                
                //Necessari ja que sino peta, no ho sabem molt be
                //Pero crec que es per elements grafics.
                try {
                    joc.coord[0]=e.getX();
                    joc.coord[1]=e.getY();
                }catch(java.lang.NullPointerException err){}
                joc.torn();
            }
            @Override
            public void mousePressed(MouseEvent e) {
                //no ho utilitzem
            }
            @Override
            public void mouseReleased(MouseEvent e) {
                //no ho utilitzem
            }
            @Override
            public void mouseEntered(MouseEvent e) {
                //no ho utilitzem
            }
            @Override
            public void mouseExited(MouseEvent e) {
                //no ho utilitzem
            }
            });
        
        //Modifiquem el system out per tal de poder imprimirlo a la app
        System.setOut(new PrintStream(new StreamCapturer(TabPanel.getCapturePane(), System.out)));
        
        
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run() { 
                   
                }
        });
        
        
    }
        
}